<?php declare(strict_types = 1);

namespace App\Monitoring\Admin;

use App\Application\Sonata\UserBundle\Admin\UserAwareAdmin;
use App\Application\Util\AutocompleteCaseInsensitiveTrait;
use App\Directory\Entity\Organization;
use App\Directory\Entity\Product;
use App\Directory\Entity\SellerAssortment;
use App\Directory\Entity\Basis;
use App\Monitoring\Entity\PriceStatistic;
use App\Monitoring\Service\PriceStatisticMethodsService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\Form\Type\DateRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Exception;
use DateTime;

/**
 * Админ класс для асортимента товаров и цен
 */
final class SellerAssortmentAdmin extends UserAwareAdmin
{
    use AutocompleteCaseInsensitiveTrait;

    /**
     * Кофигурирует родителя
     *
     * @var string
     */
    protected $parentAssociationMapping = 'organization';

    /**
     * @var PriceStatisticMethodsService
     */
    protected $priceStatisticMethodsService;

    /**
     * Конструктор класса
     *
     * @param string                       $code
     * @param string                       $class
     * @param string                       $baseControllerName
     * @param TokenStorageInterface        $tokenStorage
     * @param PriceStatisticMethodsService $priceStatistic
     */
    public function __construct($code, $class, $baseControllerName, TokenStorageInterface $tokenStorage, PriceStatisticMethodsService $priceStatistic)
    {
        $this->priceStatisticMethodsService = $priceStatistic;

        parent::__construct($code, $class, $baseControllerName, $tokenStorage);
    }

    /**
     * Устанавливает статистику по ценам
     *
     * @param SellerAssortment $object
     *
     * @throws Exception
     */
    public function preUpdate($object): void
    {
        $object->setCreatedBy($this->getCurrentUser());

        if (null !== $object->getNewPrice()) {
            $object
                ->setPriceUpdatedAt(new DateTime())
                ->setPrice($object->getNewPrice());
        }

        if (null !== $object->getNewStatus()) {
            if (PriceStatistic::STATUS_ACTIVE === $object->getNewStatus()) {
                $object->setNewPrice($object->getPrice());
            }

            $object
                ->setPriceUpdatedAt(new DateTime())
                ->setStatus($object->getNewStatus());
        }

        $price = $this->priceStatisticMethodsService->updatePriceStatisticFromAssortment($object);
        $price->setSource(PriceStatistic::SOURCE_MANUAL);

        $organisation = $object->getOrganization();
        assert($organisation instanceof Organization);

        $organisation->addPriceStatistic($price);
    }

    /**
     * Конфигурирует пути
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
            ->remove('delete')
            ->remove('show');
    }

    /**
     * Конфигурирует поля фильтров
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'basis',
                ModelAutocompleteFilter::class,
                [
                    'label' => 'Пункт отгрузки',
                    'admin_code' => 'app.directory.admin.basis',
                ],
                null,
                [
                    'minimum_input_length' => 0,
                    'class' => Basis::class,
                    'property' => 'name',
                    'callback' => $this->createCallback('name'),
                ]
            )
            ->add(
                'product',
                ModelAutocompleteFilter::class,
                [
                    'label' => 'Номенклатура',
                    'admin_code' => 'app.admin.filter.product',
                ],
                null,
                [
                    'minimum_input_length' => 0,
                    'class' => Product::class,
                    'property' => 'name',
                    'callback' => $this->createCallback('name'),
                ]
            )
            ->add(
                'deliveryType',
                null,
                [
                    'label' => 'Способ отгрузки',
                ],
                ChoiceType::class,
                [
                    'choices' => [
                        'ЖД' => SellerAssortment::DELIVERY_TYPE_RAILWAY,
                        'Авто' => SellerAssortment::DELIVERY_TYPE_CAR,
                        'Труба' => SellerAssortment::DELIVERY_TYPE_PIPE,
                        'Фасовка' => SellerAssortment::DELIVERY_TYPE_PACKING,
                        'Водный' => SellerAssortment::DELIVERY_TYPE_WATER,
                        'Без отгрузки' => SellerAssortment::DELIVERY_TYPE_EX_WORKS,
                        'Резервуар ОТП' => SellerAssortment::DELIVERY_TYPE_OPERATORS_TANK,
                    ],
                ]
            )
            ->add('priceUpdatedAt', DateRangeFilter::class, [
                'label' => 'Дата актуализации цены',
                'field_type' => DateRangePickerType::class,
            ]);
    }

    /**
     * Конфигурирует поля списка
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('basis.name', null, [
                'label' => 'Пункт отгрузки',
            ])
            ->add('product.name', null, [
                'label' => 'Номенклатура',
            ])
            ->add('deliveryType', 'choice', [
                'label' => 'Способ отгрузки',
                'disabled' => true,
                'choices' => [
                    SellerAssortment::DELIVERY_TYPE_RAILWAY => 'ЖД',
                    SellerAssortment::DELIVERY_TYPE_CAR => 'Авто',
                    SellerAssortment::DELIVERY_TYPE_PIPE => 'Труба',
                    SellerAssortment::DELIVERY_TYPE_PACKING => 'Фасовка',
                    SellerAssortment::DELIVERY_TYPE_WATER => 'Водный',
                    SellerAssortment::DELIVERY_TYPE_EX_WORKS => 'Без отгрузки',
                    SellerAssortment::DELIVERY_TYPE_OPERATORS_TANK => 'Резервуар ОТП',
                ],
            ])
            ->add('priceUpdatedAt', null, [
                'label' => 'Дата актуализации цены',
                'template' => 'Monitoring/Admin/statistic_updated_at.html.twig',
            ])
            ->add('price', null, [
                'label' => 'Старая цена',
            ])
            ->add('newPrice', null, [
                'label' => 'Новая цена',
                'editable' => true,
            ])
            ->add('newStatus', 'choice', [
                'label' => 'Статус',
                'editable' => true,
                'template' => 'Monitoring/Admin/statistic_status.html.twig',
                'virtual_field' => true,
                'choices' => [
                    SellerAssortment::STATUS_ACTIVE => 'Активный',
                    SellerAssortment::STATUS_INACTIVE => 'Неактивный',
                    SellerAssortment::STATUS_NO_CONNECTION => 'Нет связи',
                    SellerAssortment::STATUS_NO_VOLUME => 'Нет объёмов',
                    SellerAssortment::STATUS_REFUSING => 'Отказ',
                ],
            ]);
    }
}
