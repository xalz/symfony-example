<?php declare(strict_types = 1);

namespace App\Validator\Constraints;

use App\Directory\Entity\SellerAssortment;
use App\Monitoring\Repository\PriceStatisticRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;

/**
 * Класс валидации предельно допустипой цены
 */
class AssortmentPriceRangeValidator extends ConstraintValidator
{
    /**
     * Репозиторий статистики цен
     *
     * @var PriceStatisticRepository
     */
    private $priceStatisticRepository;

    /**
     * Обработчик доступов
     *
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * Смещение от текущей даты
     *
     * @var string
     */
    private $dateOffset;

    /**
     * Допустимое отклонение в процентах
     *
     * @var int
     */
    private $deviationPercent;

    /**
     * Конструктор класса
     *
     * @param AuthorizationChecker     $authChecker
     * @param PriceStatisticRepository $priceStatistic
     * @param string                   $dateOffset
     * @param int                      $deviationPercent
     */
    public function __construct(AuthorizationChecker $authChecker, PriceStatisticRepository $priceStatistic, string $dateOffset, int $deviationPercent)
    {
        $this->authorizationChecker     = $authChecker;
        $this->priceStatisticRepository = $priceStatistic;
        $this->dateOffset               = $dateOffset;
        $this->deviationPercent         = $deviationPercent;
    }

    /**
     * Валидация цены
     *
     * @param int                             $value
     * @param Constraint|AssortmentPriceRange $constraint
     *
     * @throws NonUniqueResultException
     */
    public function validate($value, Constraint $constraint): void
    {
        $object = $this->context->getObject();

        assert($constraint instanceof AssortmentPriceRange);
        assert($object instanceof SellerAssortment);

        if (!$this->context->getRoot() instanceof SellerAssortment) {
            return;
        }

        if ($object->getStatus() !== SellerAssortment::STATUS_ACTIVE) {
            return;
        }

        if ($this->authorizationChecker->isGranted('ROLE_DISABLE_PRICE_VALIDATION')) {
            return;
        }

        if (!$priceAvg = $this->getAvgPrice($object)) {
            return;
        }

        $deltaAvg = $priceAvg * $this->deviationPercent / 100;

        if ($value < $priceAvg - $deltaAvg || $value > $priceAvg + $deltaAvg) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ string }}', (string) $this->deviationPercent)
                ->addViolation();
        }
    }

    /**
     * Возвращает среднюю цену
     *
     * @param SellerAssortment $object
     *
     * @return float
     *
     * @throws NonUniqueResultException
     */
    protected function getAvgPrice(SellerAssortment $object): float
    {
        $createdAtBegin = new DateTime();
        $createdAtEnd = new DateTime();

        return $this->priceStatisticRepository->getAvgPriceForSellerAssortment(
            $object,
            $createdAtBegin->modify($this->dateOffset),
            $createdAtEnd->setTime(0, 0)
        );
    }
}
