<?php declare(strict_types = 1);

namespace App\GasStations\Controller;

use App\GasStations\Service\ActualPriceExcelProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Application\Sonata\UserBundle\Entity\User;
use App\Customers\Repository\CustomerRepository;
use App\Customers\Repository\CustomerStationRepository;
use App\Directory\Repository\ProductRepository;
use App\GasStations\Service\PriceDeltaManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Exception;

/**
 * Контроллер выгрузки отчета
 */
class ExportReportController extends AbstractController
{
    /**
     * Репозиторий номенклатуры
     *
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Репозиторий клиентов
     *
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * Репозиторий АЗС клиента
     *
     * @var CustomerStationRepository
     */
    private $customerStationRepository;

    /**
     * Сервис экспорта
     *
     * @var ActualPriceExcelProcessor
     */
    private $actualPriceProcessor;

    /**
     * Конструктор
     *
     * @param ProductRepository         $productRepository
     * @param CustomerStationRepository $customerStationRepository
     * @param CustomerRepository        $customerRepository
     * @param ActualPriceExcelProcessor $actualPriceProcessor
     */
    public function __construct(ProductRepository $productRepository, CustomerStationRepository $customerStationRepository, CustomerRepository $customerRepository, ActualPriceExcelProcessor $actualPriceProcessor)
    {
        $this->customerStationRepository = $customerStationRepository;
        $this->productRepository         = $productRepository;
        $this->customerRepository        = $customerRepository;
        $this->actualPriceProcessor      = $actualPriceProcessor;
    }

    /**
     * Выгрузка в excel
     *
     * @throws Exception
     *
     * @return BinaryFileResponse
     */
    public function excelAction()
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw $this->createAccessDeniedException();
        }

        $customer = $this->customerRepository->findOneByUser($user);
        if (is_null($customer)) {
            throw $this->createAccessDeniedException();
        }

        $stations = $this->customerStationRepository->findByCustomerIterate($customer);
        $path     = $this->actualPriceProcessor->process($stations);
        $response = new BinaryFileResponse($path);

        return $response->deleteFileAfterSend(true);
    }
}
