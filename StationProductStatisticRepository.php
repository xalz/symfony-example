<?php declare(strict_types = 1);

namespace App\GasStations\Repository;

use App\GasStations\Entity\StationProduct;
use App\GasStations\Entity\StationProductStatistic;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\Types\Type;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;

/**
 * Репозиторий статистики цен на продукт на АЗС
 *
 * @method StationProductStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method StationProductStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method StationProductStatistic[] findAll()
 * @method StationProductStatistic[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationProductStatisticRepository extends ServiceEntityRepository
{
    /**
     * Конструктор класса
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StationProductStatistic::class);
    }

    /**
     * Возвращает последнюю запись статистики соответвующей виду топлива и АЗС и дню даты обновления и по типу источника
     *
     * @param StationProduct $stationProduct
     *
     * @return StationProductStatistic|null
     *
     * @throws NonUniqueResultException
     */
    public function findOneLatestBySourceType(StationProduct $stationProduct): ?StationProductStatistic
    {
        $query = $this->createQueryBuilder('s')
            ->where('s.station = :station')
            ->andWhere('s.product = :product')
            ->andWhere('s.updatedAt >= :dayStart')
            ->andWhere('s.updatedAt <= :dayEnd')
            ->andWhere('s.sourceType = :sourceType')
            ->orderBy('s.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        return $query
            ->setParameter('station', $stationProduct->getStation())
            ->setParameter('product', $stationProduct->getProduct())
            ->setParameter('sourceType', $stationProduct->getSourceType())
            ->setParameter(
                'dayStart',
                $stationProduct->getUpdatedAt()->setTime(0, 0, 0),
                Type::DATETIME_IMMUTABLE
            )
            ->setParameter(
                'dayEnd',
                $stationProduct->getUpdatedAt()->modify('tomorrow'),
                Type::DATETIME_IMMUTABLE
            )
            ->getOneOrNullResult();
    }

    /**
     * Возвращает дельты цен за указанную дату
     *
     * @param array             $ids
     * @param DateTimeImmutable $dateTime
     *
     * @return array
     */
    public function findDeltaByGasStationIds(array $ids, DateTimeImmutable $dateTime): array
    {
        $connection = $this->getEntityManager()->getConnection();
        $tableName  = $this->getClassMetadata()->getTableName();
        $expr       = $connection->createQueryBuilder()->expr();

        $subQuery = $connection->createQueryBuilder()
            ->select('sp.price - sps.price')
            ->from($tableName, 'sps')
            ->andWhere('sps.station_id = sp.station_id')
            ->andWhere('sps.product_id = sp.product_id')
            ->andWhere('sps.source_type = sp.source_type')
            ->andWhere('sps.created_at <= :date')
            ->orderBy('sps.created_at', 'DESC')
            ->setMaxResults(1)
        ;

        $query = $connection->createQueryBuilder()
            ->select('sp.station_id, sp.product_id, sp.price')
            ->addSelect('('.$subQuery->getSQL().') as delta')
            ->from($this->getEntityManager()->getClassMetadata(StationProduct::class)->getTableName(), 'sp')
            ->andWhere($expr->in('sp.station_id', $ids ?: [0]))
            ->andWhere('sp.show_in_report = true')

            ->setParameter('date', $dateTime->format('Y-m-d H:i:s'))
            ->execute()
        ;

        assert($query instanceof Statement);

        return $query->fetchAll();
    }

    /**
     * Сохраняет запись
     *
     * @param StationProductStatistic $statistic
     *
     * @throws ORMException
     */
    public function save(StationProductStatistic $statistic): void
    {
        $this->getEntityManager()->persist($statistic);
    }
}
