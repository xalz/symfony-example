<?php declare(strict_types = 1);

namespace App\Monitoring\Controller;

use App\Directory\Entity\SellerAssortment;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Twig\Extension\SonataAdminExtension;
use Symfony\Component\HttpFoundation\JsonResponse;
use Twig\Environment;

/**
 * Контроллер ассортимента
 */
class EditablePriceController extends CRUDController
{
    /**
     * Обновление цены
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function updatePriceAction($id)
    {
        $object = $this->admin->getSubject();
        assert($object instanceof SellerAssortment);

        $value = $this->getRequest()->request->get('value');

        if (!$object instanceof SellerAssortment) {
            $error = sprintf("Entity with id '%s' not found", $id);

            return new JsonResponse($error, 404);
        }

        if (!is_string($value)) {
            return new JsonResponse('Value is not string', 400);
        }

        if (!$this->admin->hasAccess('edit', $object)) {
            return new JsonResponse('Invalid permissions', 403);
        }

        $value = str_replace([' ', ','], ['', '.'], $value);
        $object
            ->setNewPrice((float) $value)
            ->setStatus(SellerAssortment::STATUS_ACTIVE);

        $violations = $this->admin->getValidator()->validate($object, null, 'UpdatePrice');

        if ($violations->count()) {
            $messages = [];

            foreach ($violations as $violation) {
                $messages[] = $violation->getMessage();
            }

            return new JsonResponse(implode("\n", $messages), 400);
        }

        $this->admin->update($object);

        $twig = $this->container->get('twig');
        assert($twig instanceof Environment);

        $extension = $twig->getExtension(SonataAdminExtension::class);
        assert($extension instanceof SonataAdminExtension);

        $content = $extension->renderListElement(
            $twig,
            $object,
            $this->admin->getListFieldDescription('newPrice')
        );

        return new JsonResponse($content, 200);
    }
}
