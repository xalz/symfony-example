<?php declare(strict_types = 1);

namespace App\Directory\Admin;

use App\Application\Util\AutocompleteCaseInsensitiveTrait;
use App\Directory\Entity\Organization;
use App\Directory\Entity\Vic;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\Form\Type\CollectionType;
use Sonata\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Knp\Menu\ItemInterface as MenuItemInterface;

/**
 * Админ класс для организации
 */
final class OrganizationAdmin extends AbstractAdmin
{
    use AutocompleteCaseInsensitiveTrait;

    /**
     * Конфигурирует пути
     *
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('drop_dadata_update_date', $this->getRouterIdParameter().'/drop_dadata_update_date');
    }

    /**
     * Конфигурирует поля формы
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Основные данные')
                ->with('Реквизиты')
                    ->add('name', TextType::class, [
                        'label' => 'Наименование',
                        'required' => false,
                    ])
                    ->add('shortName', TextType::class, [
                        'label' => 'Краткое наименование',
                        'required' => false,
                    ])
                    ->add('inn', TextType::class, [
                        'label' => 'ИНН',
                        'required' => true,
                    ])
                    ->add('kpp', TextType::class, [
                        'label' => 'КПП',
                        'required' => false,
                    ])
                    ->add('ogrn', TextType::class, [
                        'label' => 'ОГРН',
                        'required' => false,
                    ])
                    ->add(
                        'vic',
                        ModelAutocompleteType::class,
                        [
                            'label' => 'ВИНК',
                            'class' => Vic::class,
                            'property' => 'name',
                            'required' => false,
                            'minimum_input_length' => 0,
                            'callback' => $this->createCallback('name'),
                        ],
                        [
                            'admin_code' => 'app.directory.admin.vic',
                        ]
                    )
                    ->add('type', ChoiceType::class, [
                        'label' => 'Тип',
                        'choices' => [
                            'ЮЛ' => Organization::TYPE_LEGAL,
                            'ИП' => Organization::TYPE_INDIVIDUAL,
                        ],
                    ])
                ->end()
                ->with('Дополнительно')
                    ->add('isSeller', CheckboxType::class, ['label' => 'Продавец', 'required' => false])
                ->end()
                ->with('Синхронизация с DaData')
                    ->add('dadataUpdateDate', DatePickerType::class, [
                        'label' => 'Дата',
                        'widget' => 'single_text',
                        'disabled' => true,
                    ])
                    ->add('dadataUpdateStatus', ChoiceType::class, [
                        'label' => 'Статус',
                        'disabled' => true,
                        'choices' => [
                            'Ошибка' => Organization::DADATA_STATUS_ERROR,
                            'Готово' => Organization::DADATA_STATUS_READY,
                            'В процессе' => Organization::DADATA_STATUS_PROGRESS,
                            'Новый' => Organization::DADATA_STATUS_NEW,
                        ],
                    ])
                ->end()
            ->end()
            ->tab('Комментарии')
                ->with('Комментарии')
                    ->add(
                        'comments',
                        CollectionType::class,
                        [
                            'label' => false,
                            'by_reference' => true,
                            'required' => true,
                            'type_options' => [
                                'delete' => true,
                            ],
                        ],
                        [
                            'edit' => 'inline',
                            'inline' => 'table',
                        ]
                    )
                ->end()
            ->end();
    }

    /**
     * Конфигурирует поля фильтров
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, [
                'label' => 'Наименование',
                'case_sensitive' => false,
            ])
            ->add('shortName', null, [
                'label' => 'Краткое наименование',
                'case_sensitive' => false,
            ])
            ->add('inn', null, [
                'label' => 'ИНН',
            ])
            ->add('kpp', null, [
                'label' => 'КПП',
            ])
            ->add(
                'vic',
                ModelAutocompleteFilter::class,
                [
                    'label' => 'ВИНК',
                    'admin_code' => 'app.directory.admin.vic',
                ],
                null,
                [
                    'minimum_input_length' => 0,
                    'class' => Vic::class,
                    'property' => 'name',
                    'callback' => $this->createCallback('name'),
                ]
            );
    }

    /**
     * Конфигурирует поля списка
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('shortName', null, [
                'label' => 'Краткое наименование',
                'route' => [
                    'name' => 'show',
                ],
            ])
            ->add('inn', null, ['label' => 'ИНН'])
            ->add('kpp', null, ['label' => 'КПП']);
    }

    /**
     * Конфигурирует поля просмотра
     *
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('Основные данные')
                ->with('Реквизиты', [
                    'class' => 'col-md-6',
                ])
                    ->add('name', null, [
                        'label' => 'Полное название',
                    ])
                    ->add('shortName', null, [
                        'label' => 'Краткое название',
                    ])
                    ->add('inn', null, [
                        'label' => 'ИНН',
                    ])
                    ->add('kpp', null, [
                        'label' => 'КПП',
                    ])
                    ->add('ogrn', null, [
                        'label' => 'ОГРН',
                    ])
                    ->add('vic', null, [
                        'label' => 'ВИНК',
                        'admin_code' => 'app.directory.admin.vic',
                    ])
                    ->add('type', 'choice', [
                        'label' => 'Тип',
                        'choices' => [
                            Organization::TYPE_LEGAL => 'ЮЛ',
                            Organization::TYPE_INDIVIDUAL => 'ИП',
                        ],
                    ])
                ->end()
                ->with('Дополнительно', [
                    'class' => 'col-md-6',
                ])
                    ->add('isSeller', null, [
                        'label' => 'Продавец',
                    ])
                    ->add('isMain', null, [
                        'label' => 'Головная организация',
                    ])
                ->end()
                ->with('Адрес', [
                    'class' => 'col-md-6',
                ])
                    ->add('addressEgrul', null, [
                        'label' => 'Адрес из ЕГРЮЛ',
                    ])
                    ->add('addressLegal', null, [
                        'label' => 'Юридический адрес',
                    ])
                ->end()
                ->with('Синхронизация с DaData', [
                    'class' => 'col-md-6',
                ])
                    ->add('dadataUpdateDate', null, [
                        'label' => 'Дата',
                    ])
                    ->add('dadataUpdateStatus', 'choice', [
                        'label' => 'Статус',
                        'choices' => [
                            Organization::DADATA_STATUS_ERROR => 'Ошибка',
                            Organization::DADATA_STATUS_READY => 'Готово',
                            Organization::DADATA_STATUS_PROGRESS => 'В процессе',
                            Organization::DADATA_STATUS_NEW => 'Новый',
                        ],
                    ])
                ->end()
            ->end()
            ->tab('Комментарии')
                ->with('Комментарии')
                    ->add('comments', null, [
                        'template' => 'Directory/Admin/organization_show_comments.html.twig',
                    ])
                ->end()
            ->end();
    }

    /**
     * Конфигурирует меню
     *
     * @param MenuItemInterface   $menu
     * @param string              $action
     * @param AdminInterface|null $childAdmin
     *
     * @return void
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null): void
    {
        parent::configureTabMenu($menu, $action, $childAdmin);

        if (!$childAdmin && 'edit' !== $action) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        assert($admin instanceof AdminInterface);
        $subject = $this->getSubject();
        assert($subject instanceof Organization);

        if ($id = $admin->getRequest()->get('id')) {
            $menu->addChild('Организация', [
                'uri' => $admin->generateUrl('edit', [
                    'id' => $subject->getId(),
                ]),
            ]);
            $menu->addChild('Контакты', [
                'uri' => $admin->generateUrl('app.directory.admin.organization_person.list', ['id' => $id]),
            ]);

            if ($subject->getIsSeller()) {
                $menu->addChild('Ассортимент товаров', [
                    'uri' => $admin->generateUrl('app.directory.admin.seller_assortment.list', ['id' => $id]),
                ]);
            }
        }
    }
}
