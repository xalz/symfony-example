<?php declare(strict_types = 1);

namespace App\DaData\Service;

use App\Directory\Entity\Organization;
use App\Directory\Entity\OrganizationFinance;
use App\Directory\Entity\OrganizationFounder;
use App\Directory\Entity\OrganizationLicense;
use App\Directory\Entity\OrganizationOkved;
use App\Directory\Entity\OrganizationPerson;
use App\Directory\Entity\OrganizationStatus;
use App\Directory\Repository\OrganizationRepository;
use App\Application\Util\IntegrationTrait;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;
use DateTime;

/**
 * Сервис раоты с сущностью организации
 */
class OrganizationService
{
    use IntegrationTrait;

    /**
     * Валидатор
     *
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Репозиторий организаций
     *
     * @var OrganizationRepository
     */
    private $organizationRepository;

    /**
     * Сервис работы с учредителем
     *
     * @var OrganizationFounderService
     */
    private $organizationFounderService;

    /**
     * Сервис работы с ОКВЕД
     *
     * @var OrganizationOkvedService
     */
    private $organizationOkvedService;

    /**
     * Сервисработы с сущностью статуса
     *
     * @var OrganizationStatusService
     */
    private $organizationStatusService;

    /**
     * Сервис работы с сущностью финансы
     *
     * @var OrganizationFinanceService
     */
    private $organizationFinanceService;

    /**
     * Сервис работы с сущностью лицензия
     *
     * @var OrganizationLicenseService
     */
    private $organizationLicenseService;

    /**
     * Сервис работы с сущностью контактного лица
     *
     * @var OrganizationPersonService
     */
    private $organizationPersonService;

    /**
     * Конструктор класса
     *
     * @param ValidatorInterface         $validator
     * @param OrganizationRepository     $organizationRepository
     * @param OrganizationFounderService $organizationFounderService
     * @param OrganizationOkvedService   $organizationOkvedService
     * @param OrganizationStatusService  $organizationStatusService
     * @param OrganizationFinanceService $organizationFinanceService
     * @param OrganizationLicenseService $organizationLicenseService
     * @param OrganizationPersonService  $organizationPersonService
     */
    public function __construct(ValidatorInterface $validator, OrganizationRepository $organizationRepository, OrganizationFounderService $organizationFounderService, OrganizationOkvedService $organizationOkvedService, OrganizationStatusService $organizationStatusService, OrganizationFinanceService $organizationFinanceService, OrganizationLicenseService $organizationLicenseService, OrganizationPersonService $organizationPersonService)
    {
        $this->validator                  = $validator;
        $this->organizationRepository     = $organizationRepository;
        $this->organizationFounderService = $organizationFounderService;
        $this->organizationOkvedService   = $organizationOkvedService;
        $this->organizationStatusService  = $organizationStatusService;
        $this->organizationFinanceService = $organizationFinanceService;
        $this->organizationLicenseService = $organizationLicenseService;
        $this->organizationPersonService  = $organizationPersonService;
    }

    /**
     * Возвращает поля маппинга
     *
     * @return array
     */
    public function getMappingFields(): array
    {
        return [
            'name'          => '[data][name][full_with_opf]',
            'shortName'     => [
                '[data][name][short_with_opf]',
                '[data][name][full_with_opf]',
            ],
            'ogrn'          => '[data][ogrn]',
            'addressEgrul'  => '[data][address][data][source]',
            'addressLegal'  => '[data][address][unrestricted_value]',
            'branchCount'   => '[data][branch_count]',
            'capitalValue'  => '[data][capital][value]',
            'dadataId'      => '[data][hid]',
            'employeeCount' => '[data][employee_count]',
            'kpp'           => '[data][kpp]',
            'inn'           => '[data][inn]',
            'taxSystem'     => '[data][tax_system]',
            'type'          => '[data][type]',
            'isMain'        => [
                [$this, 'getMainBranch', ['[data][branch_type]']],
            ],
            'ogrnDate'      => [
                [$this, 'getDateObject', ['[data][ogrn_date]']],
            ],
            'status'        => [
                [$this, 'getStatus', ['[data][state]']],
            ],
            'finance'       => [
                [$this, 'getFinance', ['[data][finance]']],
            ],
            'persons'       => [
                [$this, 'getPersons', ['[data][management]']],
            ],
            'founders'      => [
                [$this, 'getFounders', ['[data][founders]']],
            ],
            'okveds'        => [
                [$this, 'getOkveds', ['[data][okveds]']],
            ],
            'licenses'      => [
                [$this, 'getLicenses', ['[data][licenses]']],
            ],
        ];
    }

    /**
     * Заполняет сущность организации
     *
     * @param array        $values
     * @param Organization $organization
     *
     * @return Organization|null
     *
     * @throws Exception
     */
    public function applyEntity(array $values, Organization &$organization): ?Organization
    {
        $this->applyData($this->getMappingFields(), $values, $organization);

        $errors = $this->validator->validate($organization, null, 'DaData');
        $organization
            ->setIsValid($errors->count() === 0)
            ->setDadataUpdateDate(new DateTime());

        return $organization;
    }

    /**
     * Возвращает сущность статуса организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return OrganizationStatus|null
     */
    protected function getStatus(Organization $organization, array $values, string $path): ?OrganizationStatus
    {
        $entity = $this->getEntity($organization, $values, $path, $this->organizationStatusService);
        if ($entity instanceof OrganizationStatus) {
            $entity->setOrganization($organization);

            return $entity;
        }

        return null;
    }

    /**
     * Возвращает сущность финансов организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return OrganizationFinance|null
     */
    protected function getFinance(Organization $organization, array $values, string $path): ?OrganizationFinance
    {
        $entity = $this->getEntity($organization, $values, $path, $this->organizationFinanceService);
        if ($entity instanceof OrganizationFinance) {
            $entity->setOrganization($organization);

            return $entity;
        }

        return null;
    }

    /**
     * Возвращает признак головной организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return bool
     */
    protected function getMainBranch(Organization $organization, array $values, string $path): bool
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $branchType = $propertyAccessor->getValue($values, $path);

        return $branchType === 'MAIN' ? true : false;
    }

    /**
     * Возвращает список сущностей персонала организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return array|null
     */
    protected function getPersons(Organization $organization, array $values, string $path): ?array
    {
        $entity = $this->getEntity($organization, $values, $path, $this->organizationPersonService);
        if ($entity instanceof OrganizationPerson) {
            $entity->setOrganization($organization);
            $organization->addPerson($entity);

            return $organization->getPersons();
        }

        return null;
    }

    /**
     * Возвращает список сущностей учредителей организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return array
     */
    protected function getFounders(Organization $organization, array $values, string $path): array
    {
        return $this->getEntities(
            $organization,
            $values,
            $path,
            $this->organizationFounderService,
            function ($entity) use ($organization): OrganizationFounder {
                assert($entity instanceof OrganizationFounder);
                $entity->setOrganization($organization);

                return $entity;
            }
        );
    }

    /**
     * Возвращает список сущностей ОКВЕД организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return array
     */
    protected function getOkveds(Organization $organization, array $values, string $path): array
    {
        return $this->getEntities(
            $organization,
            $values,
            $path,
            $this->organizationOkvedService,
            function ($entity) use ($organization): OrganizationOkved {
                assert($entity instanceof OrganizationOkved);
                $entity->setOrganization($organization);

                return $entity;
            }
        );
    }

    /**
     * Возвращает список сущностей лицензий организации
     *
     * @param Organization $organization
     * @param array        $values
     * @param string       $path
     *
     * @return array
     */
    protected function getLicenses(Organization $organization, array $values, string $path): array
    {
        return $this->getEntities(
            $organization,
            $values,
            $path,
            $this->organizationLicenseService,
            function ($entity) use ($organization): OrganizationLicense {
                assert($entity instanceof OrganizationLicense);
                $entity->setOrganization($organization);

                return $entity;
            }
        );
    }

    /**
     * Возвращает массив заполненных сущностей
     *
     * @param Organization                 $organization
     * @param array                        $values
     * @param string                       $path
     * @param OrganizationServiceInterface $service
     * @param callable                     $callback
     *
     * @return object[]
     */
    protected function getEntities(Organization $organization, array $values, string $path, OrganizationServiceInterface $service, callable $callback): array
    {
        $entities = [];
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if ($values = $propertyAccessor->getValue($values, $path)) {
            foreach ($values as $value) {
                if ($entity = $service->getEntity($value, $organization)) {
                    $entities[] = $callback($entity);
                }
            }
        }

        return $entities;
    }

    /**
     * Возвращает заполненную сущность
     *
     * @param Organization                 $organization
     * @param array                        $values
     * @param string                       $path
     * @param OrganizationServiceInterface $service
     *
     * @return object|null
     */
    protected function getEntity(Organization $organization, array $values, string $path, OrganizationServiceInterface $service): ?object
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if (!$value = $propertyAccessor->getValue($values, $path)) {
            return null;
        }

        return $service->getEntity($value, $organization);
    }
}
