<?php declare(strict_types = 1);

namespace App\Directory\Repository;

use App\Directory\Entity\Organization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use DateInterval;
use DateTime;
use Exception;

/**
 * Репозиторий оргранизаций
 */
class OrganizationRepository extends ServiceEntityRepository
{
    /**
     * Интервал обнловления данных из DaData
     *
     * @var string
     */
    private $daDataUpdateInterval;

    /**
     * Конструктор класса
     *
     * @param RegistryInterface $registry
     * @param string            $daDataUpdateInterval
     */
    public function __construct(RegistryInterface $registry, string $daDataUpdateInterval)
    {
        parent::__construct($registry, Organization::class);

        $this->daDataUpdateInterval = $daDataUpdateInterval;
    }

    /**
     * Возвращает организацию по имени
     *
     * @param string $name
     *
     * @return Organization|null
     *
     * @throws NonUniqueResultException
     */
    public function findOneByName(string $name): ?Organization
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.name = :name')
            ->setParameter('name', $name)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Возвращает организацию по короткому имени
     *
     * @param string $name
     *
     * @return Organization|null
     *
     * @throws NonUniqueResultException
     */
    public function findOneByShortName(string $name): ?Organization
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.shortName = :shortName')
            ->setParameter('shortName', $name)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * Возвращает организацию для валидации уникальности
     *
     * @param string      $inn
     * @param string|null $kpp
     *
     * @return Organization|null
     *
     * @throws NonUniqueResultException
     */
    public function findOneByInnAndKpp(string $inn, ?string $kpp = null): ?Organization
    {
        $builder = $this->createQueryBuilder('p');

        $builder
            ->andWhere('p.inn = :inn')
            ->setParameter('inn', $inn);

        if (!empty($kpp)) {
            $builder
                ->andWhere('p.kpp = :kpp')
                ->setParameter('kpp', $kpp);
        }

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * Возвращает список ИНН по дате синхронизации с DaData
     *
     * @param int $limit
     *
     * @return Organization[]
     *
     * @throws Exception
     */
    public function findListForDaDataProcessing(int $limit = 40): array
    {
        $updateDate = new DateTime();
        $sub = DateInterval::createFromDateString($this->daDataUpdateInterval);
        $updateDate->sub($sub)->setTime(0, 0);
        $builder = $this->createQueryBuilder('p');

        $whereExpression = $builder->expr()->andX(
            $builder->expr()->lt('p.dadataUpdateDate', ':updateDate'),
            $builder->expr()->isNotNull('p.inn')
        );

        $query = $builder
            ->andWhere($whereExpression)
            ->orWhere('p.dadataUpdateDate IS NULL')
            ->andWhere('p.dadataUpdateStatus NOT IN(:statuses)')
            ->setParameter('updateDate', $updateDate)
            ->setParameter('statuses', [
                Organization::DADATA_STATUS_ERROR,
                Organization::DADATA_STATUS_PROGRESS,
            ])
            ->orderBy('p.dadataUpdateDate')
            ->setMaxResults($limit)
            ->getQuery();

        return $query->getResult();
    }
}
